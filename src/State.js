var EventEmitter = require("@nathanfaucett/event_emitter"),
	arrayForEach = require("@nathanfaucett/array-for_each"),
	objectMap = require("@nathanfaucett/object-map"),
	objectForEach = require("@nathanfaucett/object-for_each"),
	extend = require("@nathanfaucett/extend"),
	Store = require("./Store");

var StatePrototype;

module.exports = State;

function State() {
	EventEmitter.call(this, -1);

	this.Store = Store;

	this._stores = {};
	this._internal = {};
}
EventEmitter.extend(State);
StatePrototype = State.prototype;

State.Store = StatePrototype.Store = Store;

StatePrototype.forceUpdate = function(name) {
	this.emit("update", name);
	return this;
};

StatePrototype.forceUpdateAll = function() {
	objectForEach(this._stores, function eachStore(store) {
		store.forceUpdate();
	});
	return this.forceUpdate("*");
};

StatePrototype.state = function() {
	return this._internal;
};

StatePrototype.stateFor = function(name) {
	return this.state()[name] || {};
};

StatePrototype.setStateFor = function(name, partialState) {
	this.store(name).setState(partialState);
	return this;
};

StatePrototype.updateStateFor = function(name, updateFn) {
	this.store(name).updateState(updateFn);
	return this;
};

StatePrototype.replaceStateFor = function(name, nextState) {
	this.store(name).replaceState(nextState);
	return this;
};

StatePrototype.unsafeSetState = function(unsafeState, emit) {
	var prevInternal = this._internal,
		stores = this._stores,
		emitArray = [];

	unsafeState = Object(unsafeState);
	emit = emit !== false;

	this._internal = extend(
		{},
		prevInternal,
		objectMap(unsafeState, function mapState(nextState, name) {
			var prevState = unsafeState[name];

			if (emit) {
				emitArray.push([stores[name], prevState, nextState]);
			}

			return nextState;
		})
	);

	if (emit) {
		arrayForEach(emitArray, function eachState(args) {
			var store = args[0],
				prevState = args[1],
				nextState = args[2];

			store.emit("unsafeSetState", prevState, nextState);
		});
		this.emit("unsafeSetState", prevInternal, this._internal);
	}

	return this;
};

StatePrototype.unsafeSetStateFor = function(name, unsafeState, emit) {
	var store = this.store(name),
		prevState = this.stateFor(name),
		nextState = Object(unsafeState),
		prevInternal = this._internal,
		internal = extend({}, this._internal);

	internal[name] = nextState;

	if (emit !== false) {
		store.emit("unsafeSetStateFor", prevState, nextState);
		store.emit("unsafeSetState", prevState, nextState);

		this.emit("unsafeSetState", prevInternal, this._internal);
	}

	return this;
};

StatePrototype.store = function(name) {
	return this._stores[name];
};

StatePrototype.stores = function() {
	return this._stores;
};

StatePrototype.createStore = function(name, initialState) {
	if (this._stores[name]) {
		throw new Error("State already has store with name " + name);
	}
	var store = new this.Store(name, this),
		stores = extend({}, this._stores),
		internal = extend({}, this._internal);

	stores[name] = store;
	internal[name] = Object(initialState || {});

	this._stores = stores;
	this._internal = internal;

	this.emit("addStore", store);

	return store;
};

StatePrototype.removeStore = function(name) {
	var store = this._stores[name];

	if (!store) {
		throw new Error("State does not have store " + name);
	}
	var stores = extend({}, this._stores),
		internal = extend({}, this._internal);

	delete stores[name];
	delete internal[name];

	this._stores = stores;
	this._internal = internal;

	this.emit("removeStore", store);

	return this.forceUpdate();
};

StatePrototype.toJSON = function() {
	return JSON.parse(JSON.stringify(this._internal));
};

StatePrototype.toJS = function() {
	return this._internal;
};
