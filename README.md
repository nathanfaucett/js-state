# js-state

state management for applications

## States

a State is the single source of truth for application,
states have stores which are used to update the state.

```javascript
createStore(name: String[, initialState: Object]) -> Store
removeStore(name: String)
```

### Example

```javascript
import State from "@nathanfaucett/state";

const state = new State();
const store = state.createStore("storeName", {
    key: "value"
});

state.on("update", name => {
    console.log("Store " + name + " updated!");
    console.log(state.current());
});

store.setState({
    new_key: "value"
});
```

## Stores

stores are views into your state, they trigger updates with these methods

```javascript
setState(partialState: Object)
updateState(updateFn: Function(prevState: Object) -> Object)
replaceState(nextState: Object)
```

### Example

```javascript
import state from "./state";

const store = state.createStore("storeName", {
    key: "value"
});

console.log(store.state()); // { key: "value", new_key: "value" }

store.replaceState({
    replaced: true
});
console.log(store.state()); // { replaced: true }

store.replaceState({
    replaced: true
});
console.log(store.state()); // { replaced: true }
```

### Example Store

```javascript
import State from "@nathanfaucett/state";

const state = new State();

let ID = 0;

const todos = state.createStore("todos", {
    list: []
});

todos.create = text => {
    const id = ID++;

    todos.updateState(state => {
        const list = state.list.slice();

        list.push({
            id: id,
            text: text
        });

        return {
            list: list
        };
    });
};

todos.remove = id => {
    todos.updateState(state => {
        let list = state.list;

        const index = list.findIndex(todo => todo.id === id);

        if (index !== -1) {
            list = list.slice();
            list.slice(index, 1);
        }

        return {
            list: list
        };
    });
};
```
